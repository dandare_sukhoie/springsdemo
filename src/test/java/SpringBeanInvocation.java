import com.SpringBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created by dandare on 26/06/16.
 */
@Component
public class SpringBeanInvocation {
    @Autowired
    SpringBean springBean;

    public static void main(String[] args) {
        ApplicationContext ctx=new ClassPathXmlApplicationContext("spring.xml");
        SpringBean myDemo=ctx.getBean(SpringBean.class);
        System.out.println(myDemo.getA());
    }
}
