package com;

import org.springframework.stereotype.Component;

/**
 * Created by dandare on 26/06/16.
 */
@Component
public class SpringBean {
    int a = 10;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }
}
